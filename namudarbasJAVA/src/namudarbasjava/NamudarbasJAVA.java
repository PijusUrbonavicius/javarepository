
package namudarbasjava;

import java.util.Scanner;




public class NamudarbasJAVA {

    
    public static void main(String[] args) throws InterruptedException 
    {
      
        Scanner sc = new Scanner(System.in);
        System.out.println("           CALCULATOR 9000");
        System.out.println("Iveskite pirmaji skaiciu");
        String skaiciusA = sc.nextLine();
        Double skaiciusAA = Double.parseDouble(skaiciusA);
        System.out.println("Iveskite antraji skaiciu");
        String skaiciusB = sc.nextLine();
        Double skaiciusBB = Double.parseDouble(skaiciusB);
        
        System.out.println("Pasirinkite apskaiciavima kuri norite atlikti");
        System.out.print(" (S / Sudetis)");
        System.out.print(" (A / Atimtis)");
        System.out.print(" (D / Daugyba)");
        System.out.print(" (E / Dalyba)");
        System.out.print(" (V / Visus)");
        System.out.println("");
        String ivedimasa = sc.nextLine();
        String ivedimas = ivedimasa.toUpperCase();
        
        if (null == ivedimas)
        {
            System.out.println("Programa issijungs uz 5 sekundziu");
            Thread.sleep(5000);
            System.exit(0);
            
        }
        else switch (ivedimas) {
            case "S":
                Formules.suma(skaiciusAA, skaiciusBB);
                break;
            case "A":
                Formules.atimtis(skaiciusAA, skaiciusBB);
                break;
            case "D":
                Formules.daugyba(skaiciusAA, skaiciusBB);
                break;
            case "E":
                Formules.dalyba(skaiciusAA, skaiciusBB);
                break;
            case "V":
                Formules.Apskaiciavimai(skaiciusAA, skaiciusBB);
                break;
            default:
                System.out.println("Programa issijungs uz 5 sekundziu");
                Thread.sleep(5000);
                System.exit(0);
        }
        
        
    }
    
}
